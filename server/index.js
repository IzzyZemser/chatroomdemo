const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");

const clientURL = "http://localhost:3001";


const io = new Server(server, {
  cors: {
    origin: clientURL,
    methods: ["GET", "POST"]
  }
});
// app.get('/', (req, res) => {
//   res.send('<h1>Hello world</h1>');
// });
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });


io.on('connection', (socket) => {
    console.log('a user connected');
    socket.join('public');
    socket.on('disconnect', () => {
      console.log('user disconnected');
    });
    socket.on('chat message', (data) => {
        io.in(data.room).emit('chat message',data.msg);
        // io.emit('chat message', msg);
      });
    socket.on('join private', () => {
      socket.leave('public');
      socket.join('private');
      socket.emit("joined")
      socket.to('private').emit('user joined', socket.id);
      // io.to('private').emit(`${socket.id} has joined the room`);
    })
    socket.on('leave private', () => {
      socket.leave('private');
      socket.join('public');
      socket.emit("left private")
    })
    socket.on('typing', (room) => {
      socket.to(room).emit('type', socket.id);
    })
    socket.on('done typing', (room) => {
      socket.to(room).emit('end type', socket.id);
    })
  });

server.listen(3000, () => {
  console.log('listening on *:3000');
});