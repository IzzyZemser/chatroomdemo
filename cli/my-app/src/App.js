// import logo from './logo.svg';
import './App.css';
import styled from 'styled-components';
import {useState, useRef, useEffect} from 'react'
import { v4 as uuidv4 } from 'uuid';
import io from "socket.io-client";
const socket = io.connect("http://localhost:3000");

function App() {
  const inputRef = useRef(null);
  const [messages, set_messages] = useState([])
  const [room, set_room] = useState("public");
  const [typing, set_typing] = useState("")
  

  const submitForm = (e) => {
    e.preventDefault();

    if (inputRef.current.value) {
    socket.emit('chat message', {msg:inputRef.current.value, room});
    inputRef.current.value = '';
  }
  }

  const joinPrivate = () => {
    if(room === 'public'){
      set_room('private');
      socket.emit("join private");
    }
    
  }

  const joinPublic = () => {
    if(room === 'private'){
      set_room('public');
      socket.emit("leave private");
    }
  }

  const typeAlert = () => {
    console.log("type");
    socket.emit("typing", room);
  }
  const endTypeAlert = () =>{
    socket.emit("done typing", room);
  }

  useEffect(()=>{
    socket.on('chat message', function(msg) {
      set_messages((msgs)=>[...msgs, msg]);
    })
    socket.on('user joined', function(socket_id) {
      set_messages((msgs)=>[...msgs, `user ${socket_id} has joined the private room`]);
    })
    socket.on('joined', function() {
      set_messages((msgs)=>[...msgs, `You have successfuly joined the private room`]);
    })
    socket.on('left private', function() {
      set_messages((msgs)=>[...msgs, `You have left the private chat`]);
    })
    socket.on('type', function(socket_id) {
      console.log(typeof socket_id)
      if(socket_id){
        set_typing(`user ${socket_id} is typing...`)
      }
      
    })
    socket.on('end type', function() {
      console.log("end type")
      set_typing("")
    })
  }, [])

  return (
    <>
    <Header><h3>{typing}</h3></Header>
    <Box>
      <div className="displayChat">
        <ul id="messages">
          {messages.map((msg) => <li key={uuidv4()}>{msg}</li>)}
        </ul>
        <form id="form" action="">
          <input ref={inputRef} onFocus={typeAlert} onBlur={endTypeAlert} id="input" autoComplete="off" placeholder="write something..."/><button onClick={submitForm} >Send</button>
        </form>
      </div>
      <div className="channels">
        <button className="channel" onClick={joinPublic}>Public</button>
        <button className="channel" onClick={joinPrivate}>Private</button>
        <button className="channel" data-room="tech">Tech</button>
      </div>  


      
    </Box>
    </>
  );
}

export default App;

const Header = styled.div`
  margin:5px;
`;

const Box = styled.div`
  display:flex;
  ul{
    list-style-type: none;
  }

  .channels{
    display:flex;
    flex-direction:column;
    margin-top:20px;
    margin-right:10px;
    font-size:2rem;
  }
  .channel{
    margin: 0 0 10px 10px;
    font-size: 1.5rem;
  }

  button{
    background: #333;
    border: none;
    padding: 0 1rem;
    margin: 0.25rem;
    border-radius: 3px;
    outline: none;
    color: #fff;
  }
  .displayChat{
    flex-basis:90%;
    height: 75vh;
    margin: 10px;
    border: 1px solid black;
   
    
  }
  form{
    background: rgba(0, 0, 0, 0.15);
    padding: 0.25rem;
    display: flex;
    height: 3rem;
    box-sizing: border-box;
    backdrop-filter: blur(10px);
    position: absolute;
    bottom: 0;
    width: 100%;

    input{
      border: none;
      padding: 0 1rem;
      flex-grow: 1;
      border-radius: 2rem;
      margin: 0.25rem;
    }
  }
`;
